import org.gradle.api.internal.tasks.userinput.UserInputHandler

var inputHeight : Int = -1

open class InputHeightTask @Inject constructor() : DefaultTask() {

    @Input
    var enteredHeight: Int = -1

    @TaskAction
    fun askQuestion() {
        enteredHeight = services
            .get(UserInputHandler::class.java)
            .askQuestion("Input your height:", "180")
            .toInt()
        println("Entered $enteredHeight")
    }

}

val heightInputTask = tasks.register<InputHeightTask>("inputHeight") {
    doLast { inputHeight = enteredHeight }
}

val heightCaculationTask = tasks.register("doHeightCaculation") {
    dependsOn(heightInputTask)
    doFirst {
        println("Caculating...")
    }
}

val furtherHeightCaculationTask = tasks.register("doFurtherHeightCaculation") {
    dependsOn(heightCaculationTask)
    doLast{
        Thread.sleep(4000)
    }
}

val heightOutputTask = tasks.register("reportHeight") {
    dependsOn(furtherHeightCaculationTask)
    doLast {
        println("Your height is ${inputHeight}cm!")
    }
}

tasks.register("caculateHeight") {
    dependsOn(heightOutputTask)
}

tasks.register("calculateHeight") {
    throw IllegalArgumentException("Command 'calculateHeight' is not supported. Did you mean 'caculateHeight'?")
}
