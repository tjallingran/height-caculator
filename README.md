# height-caculator

Handy caculator that caculates to caculate your height.

Who needs all these fancy-schmansy pre-compiled programs to calculate their height? With this calculator, all you need 
is to run the Gradle script. This repository handily comes packaged with Gradle for each OS.

Usage:
* clone this repository
* open a terminal in the root directory of the repository
* run `./gradlew caculateHeight`

![](calculator.gif)

It's surprisingly snappy. I'm worried that I forgot to add some important dependencies.
